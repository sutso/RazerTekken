﻿namespace TekkenLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using TekkenLib.Characters;

    public class TekkenProcess
    {
        private GameStateMachine _gameStateMachine;

        public delegate void P1CharacterSelectionChangedEventHandler(object sender, CharacterChangedEventArgs e);
        public event P1CharacterSelectionChangedEventHandler P1CharacterSelectionChanged;

        public delegate void P1HealthChangedEventHandler(object sender, HealthChangedEventArgs e);
        public event P1HealthChangedEventHandler P1HealthChanged;

        private const int P1CharSelectOffset = 0x33cdbe8;
        private const int P1Offset = 0x33f6b40;
        private const int P1HealthOffset = 0x11D8;
        private const int P1RageOffset = 0x99a;
        private const int P1ThrowTechOffset = 0x3ec;

        private readonly IntPtr[] _p1CharSelectPointers = { new IntPtr(P1CharSelectOffset), new IntPtr(0x80), new IntPtr(0x3cc) };
        private readonly IntPtr[] _p1HealthPointers = { new IntPtr(P1Offset), new IntPtr(0), new IntPtr(P1HealthOffset) };

        private readonly long _baseAddress;
        private readonly VaMemory _vaMemory;

        private int P1Health { get; set; }
        public bool P1Rage { get; private set; }
        public int P1ThrowTech { get; private set; }
        private int SelectedChar { get; set; }

        public TekkenProcess(string processName)
        {
            _vaMemory = new VaMemory(processName);

            if (!_vaMemory.CheckProcess())
                throw new Exception("Game window not found!");

            _baseAddress = _vaMemory.GetBaseAddress;
            _gameStateMachine = new GameStateMachine();
        }

        public void Update()
        {
            UpdateP1();
            UpdateGameState();
        }

        public void UpdateGameState()
        {
            CharSelect();
        }

        private void CharSelect()
        {
            var newValue = _vaMemory.ReadMultipleToInt32(_p1CharSelectPointers);

            if (SelectedChar != newValue)
            {
                SelectedChar = newValue;
                var singleOrDefault = CharacterCollection.Characters.Single(x => x.CharacterId == newValue);
                OnP1CharacterSelectionChanged(singleOrDefault);
            }
        }

        private void UpdateP1()
        {
            var newValue = _vaMemory.ReadMultipleToInt32(_p1HealthPointers);

            if (P1Health != newValue)
            {
                var resetHealthBar = P1Health <= 0 && P1Health < newValue;

                P1Health = newValue;
                OnP1HealthChanged(new HealthChangedEventArgs(newValue, resetHealthBar));
            }
        }


        private void OnP1CharacterSelectionChanged(CharacterBase characterBase)
        {
            P1CharacterSelectionChanged?.Invoke(this, new CharacterChangedEventArgs(characterBase));
        }

        private void OnP1HealthChanged(HealthChangedEventArgs e)
        {
            P1HealthChanged?.Invoke(this, e);
        }
    }

    public class HealthChangedEventArgs : EventArgs
    {
        public int Health { get; }
        public bool ResetHealthBar { get; }

        public HealthChangedEventArgs(int health, bool resetHealthBar)
        {
            Health = health;
            ResetHealthBar = resetHealthBar;
        }
    }

    public class CharacterChangedEventArgs : EventArgs
    {
        public CharacterBase Character { get; }

        public CharacterChangedEventArgs(CharacterBase characterBase)
        {
            Character = characterBase;
        }
    }

    public static class VaMemoryExtensions
    {
        public static int ReadMultipleToInt32(this VaMemory memory, params IntPtr[] offsets)
        {
            long temp = memory.ReadInt64(IntPtr.Add(new IntPtr(memory.GetBaseAddress), offsets[0].ToInt32()));

            for (var i = 1; i < offsets.Length; i++)
            {
                temp = memory.ReadInt64(IntPtr.Add((IntPtr)temp, offsets[i].ToInt32()));
            }

            return (int)temp;
        }
    }
}