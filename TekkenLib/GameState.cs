﻿namespace TekkenLib
{
    public static class GameStateEnums
    {
        public enum GameState
        {
            None,
            MainMenu,
            CharacterSelect,
            StageSelect,
            Ingame,
        }

        public enum GameMode
        {
            None,
            Ranked,
            Vs,
            Treasure,
            Arcade
        }
    }
}