﻿namespace TekkenLib
{
    public sealed class GameStateMachine
    {
        public GameStateEnums.GameState GameState { get; set; }
        public GameStateEnums.GameMode GameMode { get; set; }

        public delegate void GameStateChangedEventHandler(object sender, GameStateChangedEventArgs e);
        public event GameStateChangedEventHandler GameStateChanged;

        public GameStateMachine()
        {
            GameState = GameStateEnums.GameState.None;
            GameMode = GameStateEnums.GameMode.None;
        }

        private void OnGameStateChanged(GameStateChangedEventArgs e)
        {
            GameStateChanged?.Invoke(this, e);
        }

        public void ChangeGameState(GameStateEnums.GameState newGameState, GameStateEnums.GameMode newGameMode)
        {
            var oldGameState = GameState;
            var oldGameMode = GameMode;

            GameState = newGameState;
            GameMode = newGameMode;

            OnGameStateChanged(new GameStateChangedEventArgs(oldGameState, newGameState, oldGameMode, newGameMode));
        }
    }
}
