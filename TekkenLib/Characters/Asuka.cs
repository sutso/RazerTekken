﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Asuka : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Red;
        public override int CharacterId { get; } = 12;
    }
}
