﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class MasterRaven : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.DeepBlue;
        public override int CharacterId { get; } = 29;
    }
}
