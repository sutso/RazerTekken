﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Jack7 : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Black;
        public override Color SecondaryColor { get; } = Color.Green;
        public override int CharacterId { get; } = 11;
    }
}
