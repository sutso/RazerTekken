﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Kuma : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Brown;
        public override Color SecondaryColor { get; } = Color.Brown;
        public override int CharacterId { get; } = 33;
    }
}
