﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Jin : CharacterBase
    {
        public override Color BaseColor { get; } = Color.DeepBlue;
        public override Color SecondaryColor { get; } = Color.Red;
        public override int CharacterId { get; } = 6;
    }
}
