﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Dragunov : CharacterBase
    {
        public override Color BaseColor { get; } = Color.DeepBlue;
        public override Color SecondaryColor { get; } = Color.Black;
        public override int CharacterId { get; } = 16;
    }
}
