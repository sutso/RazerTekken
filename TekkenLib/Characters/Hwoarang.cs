﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Hwoarang : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Blue;
        public override int CharacterId { get; } = 4;
    }
}
