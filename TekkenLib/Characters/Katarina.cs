﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Katarina : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Orange;
        public override int CharacterId { get; } = 21;
    }
}
