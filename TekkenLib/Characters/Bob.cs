﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Bob : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Blue;
        public override Color SecondaryColor { get; } = Color.Red;
        public override int CharacterId { get; } = 31;
    }
}
