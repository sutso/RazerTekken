﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Leo : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Blue;
        public override int CharacterId { get; } = 17;
    }
}
