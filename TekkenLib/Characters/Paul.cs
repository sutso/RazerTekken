﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Paul : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.Blue;
        public override int CharacterId { get; } = 0;
    }
}
