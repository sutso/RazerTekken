﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Lee : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Purple;
        public override Color SecondaryColor { get; } = Color.White;
        public override int CharacterId { get; } = 30;
    }
}
