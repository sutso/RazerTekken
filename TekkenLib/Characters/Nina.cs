﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Nina : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Black;
        public override int CharacterId { get; } = 28;
    }
}
