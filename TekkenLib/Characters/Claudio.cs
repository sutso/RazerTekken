﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Claudio : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.White;
        public override int CharacterId { get; } = 20;
    }
}
