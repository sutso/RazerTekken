﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Josie : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Yellow;
        public override Color SecondaryColor { get; } = Color.DeepBlue;
        public override int CharacterId { get; } = 24;
    }
}
