﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Heihachi : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.Orange;
        public override int CharacterId { get; } = 8;
    }
}
