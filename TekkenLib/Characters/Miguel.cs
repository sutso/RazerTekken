﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Miguel : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.Brown;
        public override int CharacterId { get; } = 37;
    }
}
