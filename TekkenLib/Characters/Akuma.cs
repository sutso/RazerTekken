﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Akuma : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Purple;
        public override Color SecondaryColor { get; } = Color.Black;
        public override int CharacterId { get; } = 32;
    }
}
