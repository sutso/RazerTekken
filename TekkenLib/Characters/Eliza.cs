﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Eliza : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.Black;
        public override int CharacterId { get; } = 36;
    }
}
