﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Xiaoyou : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Yellow;
        public override Color SecondaryColor { get; } = Color.Orange;
        public override int CharacterId { get; } = 5;
    }
}
