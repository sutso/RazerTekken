﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Gigas : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.FromRgb(0x101010);
        public override int CharacterId { get; } = 25;
    }
}
