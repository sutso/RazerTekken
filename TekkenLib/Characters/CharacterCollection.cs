﻿namespace TekkenLib.Characters
{
    using System.Collections.Generic;



    public static class CharacterCollection
    {
        internal static List<CharacterBase> Characters { get; set; } = new List<CharacterBase>();

        static CharacterCollection()
        {
            Characters.Add(new Leo());
            Characters.Add(new Alisa());
            Characters.Add(new Gigas());
            Characters.Add(new Katarina());
            Characters.Add(new DevilJin());
            Characters.Add(new Feng());
            Characters.Add(new Jin());
            Characters.Add(new Josie());
            Characters.Add(new Akuma());
            Characters.Add(new Asuka());
            Characters.Add(new Bob());
            Characters.Add(new Bryan());
            Characters.Add(new Chloe());
            Characters.Add(new Claudio());
            Characters.Add(new Dragunov());
            Characters.Add(new Eddy());
            Characters.Add(new Eliza());
            Characters.Add(new Heihachi());
            Characters.Add(new Hwoarang());
            Characters.Add(new Jack7());
            Characters.Add(new Kazumi());
            Characters.Add(new Kazuya());
            Characters.Add(new King());
            Characters.Add(new Kuma());
            Characters.Add(new Lars());
            Characters.Add(new Law());
            Characters.Add(new Lee());
            Characters.Add(new Lili());
            Characters.Add(new MasterRaven());
            Characters.Add(new Miguel());
            Characters.Add(new Nina());
            Characters.Add(new Panda());
            Characters.Add(new Paul());
            Characters.Add(new Shaheen());
            Characters.Add(new Steve());
            Characters.Add(new Unknown());
            Characters.Add(new Xiaoyou());
            Characters.Add(new Yoshimitsu());
        }
    }
}
