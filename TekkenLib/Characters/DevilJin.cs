﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class DevilJin : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Black;
        public override Color SecondaryColor { get; } = Color.White;
        public override int CharacterId { get; } = 13;
    }
}
