﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Eddy : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Brown;
        public override Color SecondaryColor { get; } = Color.White;
        public override int CharacterId { get; } = 35;
    }
}
