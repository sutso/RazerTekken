﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Yoshimitsu : CharacterBase
    {
        public override Color BaseColor { get; } = Color.White;
        public override Color SecondaryColor { get; } = Color.Green;
        public override int CharacterId { get; } = 3;
    }
}
