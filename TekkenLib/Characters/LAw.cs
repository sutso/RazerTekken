﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Law : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Yellow;
        public override Color SecondaryColor { get; } = Color.Black;
        public override int CharacterId { get; } = 1;
    }
}
