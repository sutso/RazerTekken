﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Bryan : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Green;
        public override Color SecondaryColor { get; } = Color.Brown;
        public override int CharacterId { get; } = 7;
    }
}
