﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public abstract class CharacterBase
    {
        public virtual Color BaseColor { get; } = Color.White;
        public virtual Color SecondaryColor { get; } = Color.Red;
        public abstract int CharacterId { get; }
    }
}
