﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Unknown : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Red;
        public override Color SecondaryColor { get; } = Color.Red;
        public override int CharacterId { get; } = 255;
    }
}
