﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class Chloe : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Pink;
        public override Color SecondaryColor { get; } = Color.White;
        public override int CharacterId { get; } = 22;
    }
}
