﻿namespace TekkenLib.Characters
{
    using Corale.Colore.Core;

    public class King : CharacterBase
    {
        public override Color BaseColor { get; } = Color.Yellow;
        public override Color SecondaryColor { get; } = Color.Blue;
        public override int CharacterId { get; } = 2;
    }
}
