﻿using System;

namespace TekkenLib
{
    public class GameStateChangedEventArgs : EventArgs
    {
        public GameStateEnums.GameState OldGameState { get; }
        public GameStateEnums.GameState NewGameState { get; }
        public GameStateEnums.GameMode OldGameMode { get; }
        public GameStateEnums.GameMode NewGameMode { get; }

        public GameStateChangedEventArgs(
            GameStateEnums.GameState oldGameState,
            GameStateEnums.GameState newGameGameState,
            GameStateEnums.GameMode oldGameMode,
            GameStateEnums.GameMode newGameMode)
        {
            OldGameState = oldGameState;
            NewGameState = newGameGameState;
            OldGameMode = oldGameMode;
            NewGameMode = newGameMode;
        }
    }
}