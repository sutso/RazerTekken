﻿using System;
using System.Globalization;
using System.Threading;

using Corale.Colore.Core;
using Corale.Colore.Layering.Animation;
using Corale.Colore.Layering.Layer;
using Corale.Colore.Razer.Keyboard;
using Corale.Colore.Razer.Keyboard.Effects;

namespace RazerTekken
{
    class Program
    {
        static void Main()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

            //var baseLayer = new Layer(DefaultAnimations.Wave(Color.Red, Color.Yellow, 50));

            //var activeKeyLayer = Custom.Create();
            //activeKeyLayer[Key.W] = Color.Green;
            //activeKeyLayer[Key.A] = Color.Green;
            //activeKeyLayer[Key.S] = Color.Green;
            //activeKeyLayer[Key.D] = Color.Green;

            //var dynamicIndicatorLayer = new Layer(DefaultAnimations.StaticFrame(new Custom(Color.Black)));

            //LayerManager.Instance.AddLayer(LayerHeight.Base, baseLayer);
            //LayerManager.Instance.AddLayer(LayerHeight.ActiveKeys, new Layer(activeKeyLayer));
            //LayerManager.Instance.AddLayer(LayerHeight.DynamicIndicators, dynamicIndicatorLayer);

            //LayerManager.ApplyLayers();

            ////Thread.Sleep(5000);

            ////layer = Custom.Create();
            ////layer[Key.LeftShift] = Color.Green;
            ////layer[Key.Logo] = Color.Green;
            ////layer[Key.Space] = Color.Green;
            ////layer[Key.Enter] = Color.Green;

            ////RazerEffectLayerManager.Instance.AddLayer(RazerLayerHeight.DynamicIndicators, new Layer.Layer(layer));
            ////RazerEffectLayerManager.Instance.AddLayer(RazerLayerHeight.ActiveKeys, new Layer.Layer(DefaultAnimations.InsideOut()));

            TekkenChromaService service = new TekkenChromaService();
            service.Run();
            Console.ReadKey();
        }
    }
}
