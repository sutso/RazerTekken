﻿using Corale.Colore.Core;
using Corale.Colore.Razer.Keyboard.Effects;

namespace RazerTekken
{
    using System;
    using System.Threading.Tasks;

    using Corale.Colore.Layering.Animation;
    using Corale.Colore.Layering.Layer;

    using TekkenLib;

    public class TekkenChromaService
    {
        private TekkenProcess _tekkenProcess;

        public TekkenChromaService()
        {
            Init();
        }

        private void Init()
        {
            try
            {
                _tekkenProcess = new TekkenProcess("TekkenGame-Win64-Shipping");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

            Console.WriteLine("Game found. Have fun!");
            LayerManager.Instance.AddLayer(LayerHeight.Base, new Layer(new Custom(Color.Red)));
            LayerManager.ApplyLayers();
            _tekkenProcess.P1CharacterSelectionChanged += TekkenProcessOnP1CharacterSelectionChanged;
            _tekkenProcess.P1HealthChanged += TekkenProcessOnP1HealthChanged;
        }

        private void TekkenProcessOnP1HealthChanged(object sender, HealthChangedEventArgs healthChangedEventArgs)
        {
            if (healthChangedEventArgs.ResetHealthBar)
            {
                Console.WriteLine(healthChangedEventArgs.Health);
                LayerManager.Instance.AddLayer(LayerHeight.ActiveKeys,
                    new Layer(
                        DefaultAnimations.ResetHealthBar()));
            }
            else
            {
                Console.WriteLine(healthChangedEventArgs.Health);
                LayerManager.Instance.AddLayer(LayerHeight.ActiveKeys,
                    new Layer(
                        DefaultAnimations.SetHealthBar(healthChangedEventArgs.Health)));
            }

            LayerManager.ApplyLayers();
        }

        private void TekkenProcessOnP1CharacterSelectionChanged(object sender, CharacterChangedEventArgs characterChangedEventArgs)
        {
            var selectedCharacter = characterChangedEventArgs.Character;
            LayerManager.Instance.AddLayer(LayerHeight.Base,
                new Layer(
                    DefaultAnimations.FadeBetween(selectedCharacter.BaseColor, selectedCharacter.SecondaryColor, 10, 100))
                    );

            LayerManager.ApplyLayers();
        }

        // ReSharper disable once ObjectCreationAsStatement
        public void Run()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    try
                    {
                        _tekkenProcess.Update();
                        await Task.Delay(250);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            });

        }
    }
}
