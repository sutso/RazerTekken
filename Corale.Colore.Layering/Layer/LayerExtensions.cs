﻿namespace Corale.Colore.Layering.Layer
{
    using Corale.Colore.Core;
    using Corale.Colore.Razer.Keyboard;
    using Corale.Colore.Razer.Keyboard.Effects;

    public static class LayerExtensions
    {
        public static void ApplyEffect(this Custom baseLayer, Custom layerToApply)
        {
            for (var i = 0; i < Constants.MaxKeys; ++i)
            {
                var keyToApplyHasColor = (layerToApply[i] != Color.Black);

                if (keyToApplyHasColor)
                {
                    baseLayer[i] = layerToApply[i];
                }
            }
        }
    }
}