﻿using Corale.Colore.Layering.Layer.Frames;

namespace Corale.Colore.Layering.Layer
{
    using System.Collections.Generic;

    using Corale.Colore.Layering.Animation;
    using Corale.Colore.Razer.Keyboard.Effects;

    public class Layer
    {
        public List<FrameBase> Frames { get; }

        public Layer(List<FrameBase> frames)
        {
            Frames = frames;
        }

        public Layer(Custom customEffect)
        {
            Frames = DefaultAnimations.StaticFrame(customEffect);
        }
    }
}
