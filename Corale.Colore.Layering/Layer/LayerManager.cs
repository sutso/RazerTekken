﻿using Corale.Colore.Layering.Layer.Frames;

namespace Corale.Colore.Layering.Layer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Corale.Colore.Core;
    using Corale.Colore.Razer.Keyboard.Effects;

    public class LayerManager
    {
        private static bool IsRunning { get; set; }
        private static readonly object Mutex = new object();

        private static LayerManager _instance;
        public static LayerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Mutex)
                    {
                        {
                            _instance = new LayerManager();
                        }
                    }
                }

                return _instance;
            }
        }

        private static int MaxFrameSize
        {
            get
            {
                return CustomEffects.Sum(customEffect => customEffect.Value.Frames.Count);
            }
        }

        private static Task _renderTask;
        private static CancellationTokenSource _cancellationTokenSource;
        private static CancellationToken _cancellationToken;
        private static readonly Dictionary<LayerHeight, Layer> CustomEffects = new Dictionary<LayerHeight, Layer>();

        public void AddLayer(LayerHeight layerHeight, Layer layer)
        {
            if ((int)layerHeight > 3 || (int)layerHeight <= 0)
                throw new ArgumentOutOfRangeException(nameof(layerHeight));

            CustomEffects[layerHeight] = layer ?? throw new ArgumentNullException(nameof(layer));
        }

        private void Stop()
        {
            if (_cancellationToken.CanBeCanceled)
                _cancellationTokenSource.Cancel();

            _renderTask.Wait();
            IsRunning = false;
        }

        private LayerManager()
        {
            AddLayer(LayerHeight.Base, new Layer(new Custom(Color.Black)));
            AddLayer(LayerHeight.ActiveKeys, new Layer(new Custom(Color.Black)));
            AddLayer(LayerHeight.DynamicIndicators, new Layer(new Custom(Color.Black)));
        }

        public static void ApplyLayers()
        {
            if (IsRunning)
                return;

            Frame previousBaseFrame = null;
            Frame previousActiveKeyFrame = null;
            Frame previousDynamicKeyFrame = null;

            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;

            _renderTask = new Task(() =>
            {
                Custom bufferedCustom = Custom.Create();

                while (true)
                {
                    Thread.Sleep(10);
                    for (var i = 0; i < MaxFrameSize; i++)
                    {
                        if (_cancellationToken.IsCancellationRequested)
                            return;

                        var currentBaseFrame = GetCurrentFrameFor(LayerHeight.Base, i);
                        var currentActiveKeyFrame = GetCurrentFrameFor(LayerHeight.ActiveKeys, i);
                        var currentDynamicIndicatorsFrame = GetCurrentFrameFor(LayerHeight.DynamicIndicators, i);

                        Custom baseLayerCopy;

                        baseLayerCopy = currentBaseFrame is WaitFrame ?
                            Custom.Create() :
                            currentBaseFrame.ActiveKeysInFrame.Clone();

                        previousBaseFrame = ProcessFrame(previousBaseFrame, currentBaseFrame, baseLayerCopy);
                        previousActiveKeyFrame = ProcessFrame(previousActiveKeyFrame, currentActiveKeyFrame, baseLayerCopy);
                        previousDynamicKeyFrame = ProcessFrame(previousDynamicKeyFrame, currentDynamicIndicatorsFrame, baseLayerCopy);

                        if (!bufferedCustom.Equals(baseLayerCopy))
                            Keyboard.Instance.SetCustom(baseLayerCopy);

                        bufferedCustom = baseLayerCopy;
                    }
                }
            }, _cancellationToken);

            _renderTask.Start();
            IsRunning = true;
        }

        private static Frame ProcessFrame(Frame previousFrame, FrameBase currentFrame, Custom baseLayer)
        {
            switch (currentFrame)
            {
                case WaitFrame frame:
                    Thread.Sleep(frame.WaitingTime);
                    if (previousFrame != null)
                        baseLayer.ApplyEffect(previousFrame.ActiveKeysInFrame);
                    break;
                case Frame frame:
                    baseLayer.ApplyEffect(frame.ActiveKeysInFrame);
                    previousFrame = frame;
                    break;
                case OnceFrame frame:
                    if (!frame.Used)
                    {
                        baseLayer.ApplyEffect(frame.ActiveKeysInFrame);
                        frame.Used = true;
                    }
                    break;
            }

            return previousFrame;
        }

        private static FrameBase GetCurrentFrameFor(LayerHeight layerHeight, int i)
        {
            return CustomEffects[layerHeight].Frames.ElementAtOrDefault(i) == null
                    ? CustomEffects[layerHeight].Frames[0]
                    : CustomEffects[layerHeight].Frames[i];
        }
    }
}