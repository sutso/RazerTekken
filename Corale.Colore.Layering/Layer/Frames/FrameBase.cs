﻿using Corale.Colore.Razer.Keyboard.Effects;

namespace Corale.Colore.Layering.Layer.Frames
{
    public class FrameBase
    {
        public readonly Custom ActiveKeysInFrame;

        protected FrameBase(Custom activeKeysInFrame)
        {
            ActiveKeysInFrame = activeKeysInFrame;
        }

        protected FrameBase()
        {
            
        }
    }
}