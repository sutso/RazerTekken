﻿using Corale.Colore.Razer.Keyboard.Effects;

namespace Corale.Colore.Layering.Layer.Frames
{
    public abstract class OnceFrame : FrameBase
    {
        protected OnceFrame(Custom activeKeysInFrame) : base(activeKeysInFrame)
        {
        }

        public bool Used { get; set; }
    }
}
