﻿namespace Corale.Colore.Layering.Layer.Frames
{
    public class WaitFrame : FrameBase
    {
        public int WaitingTime { get; }

        public WaitFrame(int waitingTime)
        {
            WaitingTime = waitingTime;
        }
    }
}
