﻿namespace Corale.Colore.Layering.Layer
{
    public enum LayerHeight
    {
        Base = 1,
        ActiveKeys = 2,
        DynamicIndicators = 3
    }
}