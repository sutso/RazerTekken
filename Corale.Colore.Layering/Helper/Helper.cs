﻿using Corale.Colore.Core;

namespace Corale.Colore.Layering.Helper
{
    static class Helper
    {
        public static Color Lerp(this Color a, Color b, double t)
        {
            t = Clamp01(t);

            var red = (byte)(a.R + (b.R - a.R) * t);
            var green = (byte)(a.G + (b.G - a.G) * t);
            var blue = (byte)(a.B + (b.B - a.B) * t);

            return new Color(red, green, blue);
        }

        private static double Clamp01(double value)
        {
            double result;
            if (value < 0)
            {
                result = 0;
            }
            else if (value > 1)
            {
                result = 1;
            }
            else
            {
                result = value;
            }
            return result;
        }
    }
}
