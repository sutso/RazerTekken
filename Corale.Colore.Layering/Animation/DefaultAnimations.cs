﻿// ReSharper disable RedundantCast

using Corale.Colore.Layering.Helper;
using Corale.Colore.Layering.Layer.Frames;

namespace Corale.Colore.Layering.Animation
{
    using System;
    using System.Collections.Generic;

    using Corale.Colore.Core;
    using Corale.Colore.Razer.Keyboard;
    using Corale.Colore.Razer.Keyboard.Effects;

    public static class DefaultAnimations
    {
        public static List<FrameBase> InsideOut()
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            int r, c;

            const int Columns = 22;

            for (c = 0; c < Columns / 2; c++)
            {
                for (r = 1; r < 6; r++)
                {
                    var currentColumnX = (Columns / 2) - c;
                    var currentColumnZ = (Columns / 2) + c;

                    customEffect[r, currentColumnZ] = Color.Blue.Value;
                    customEffect[r, currentColumnX] = Color.Blue.Value;

                    listOfFrames.Add(new Frame(customEffect.Clone()));
                }

                listOfFrames.Add(new WaitFrame(50));
            }

            for (c = 0; c < Columns / 2; c++)
            {
                for (r = 1; r < 6; r++)
                {
                    var currentColumnX = (Columns / 2) - c;
                    var currentColumnZ = (Columns / 2) + c;

                    customEffect[r, currentColumnZ] = Color.Yellow.Value;
                    customEffect[r, currentColumnX] = Color.Yellow.Value;

                    listOfFrames.Add(new Frame(customEffect.Clone()));
                }

                listOfFrames.Add(new WaitFrame(50));
            }

            for (c = 0; c < Columns / 2; c++)
            {
                for (r = 1; r < 6; r++)
                {
                    var currentColumnX = (Columns / 2) - c;
                    var currentColumnZ = (Columns / 2) + c;

                    customEffect[r, currentColumnZ] = Color.Purple.Value;
                    customEffect[r, currentColumnX] = Color.Purple.Value;

                    listOfFrames.Add(new Frame(customEffect.Clone()));
                }

                listOfFrames.Add(new WaitFrame(50));
            }

            for (c = 0; c < Columns / 2; c++)
            {
                for (r = 1; r < 6; r++)
                {
                    var currentColumnX = (Columns / 2) - c;
                    var currentColumnZ = (Columns / 2) + c;

                    customEffect[r, currentColumnZ] = Color.White.Value;
                    customEffect[r, currentColumnX] = Color.White.Value;

                    listOfFrames.Add(new Frame(customEffect.Clone()));
                }

                listOfFrames.Add(new WaitFrame(50));
            }

            return listOfFrames;
        }

        public static List<FrameBase> FadeBetween(Color color1, Color color2, int updateInterval, int frameCount)
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            Color lastColor = new Color(0, 0, 0);

            long fadeCount = frameCount;

            for (long i = 0; i <= fadeCount; i++)
            {
                var percentage = (double)1 / (double)fadeCount * i;

                var interpolate = color1.Lerp(color2, percentage);

                customEffect.Set(interpolate);
                lastColor = interpolate;
                listOfFrames.Add(new Frame(customEffect.Clone()));
                listOfFrames.Add(new WaitFrame(updateInterval));
            }

            Color lastColorToUse = new Color(lastColor);

            for (long i = 0; i <= fadeCount; i++)
            {
                var percentage = (double)1 / (double)fadeCount * i;

                var interpolate = lastColorToUse.Lerp(color1, percentage);

                customEffect.Set(interpolate);
                listOfFrames.Add(new Frame(customEffect.Clone()));
                listOfFrames.Add(new WaitFrame(updateInterval));
            }

            return listOfFrames;
        }

        public static List<FrameBase> Starlight(Color keyColor, Color fadeColor)
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            var rand = new Random();

            var keyIndex = rand.Next(0, Constants.MaxKeys);
            Color lastColor = new Color(0, 0, 0);


            long fadeCount = 5;

            for (long i = 0; i <= fadeCount; i++)
            {
                var percentage = (double)1 / (double)fadeCount * i;

                var interpolate = keyColor.Lerp(fadeColor, percentage);

                customEffect[keyIndex] = interpolate;
                lastColor = interpolate;
                listOfFrames.Add(new Frame(customEffect.Clone()));
                listOfFrames.Add(new WaitFrame(20));
            }

            Color lastColorToUse = new Color(lastColor);

            for (long i = 0; i <= fadeCount; i++)
            {
                var percentage = (double)1 / (double)fadeCount * i;

                var interpolate = lastColorToUse.Lerp(keyColor, percentage);

                customEffect[keyIndex] = interpolate;
                listOfFrames.Add(new Frame(customEffect.Clone()));
                listOfFrames.Add(new WaitFrame(20));
            }

            return listOfFrames;
        }

        public static List<FrameBase> StaticFrame(Custom customEffect)
        {
            var frame = new Frame(customEffect);
            return new List<FrameBase> { frame };
        }

        public static List<FrameBase> Wave(Color fromColor, Color? toColor, int updateInterval)
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            if (toColor == null)
                toColor = fromColor;

            int c;

            for (c = 0; c < 22; c++)
            {
                var percentage = (double)1 / (double)(22) * c;
                int r;
                for (r = 0; r < 6; r++)
                {
                    customEffect[r, c] = fromColor.Lerp(toColor.GetValueOrDefault(), percentage);
                    listOfFrames.Add(new Frame(customEffect.Clone()));
                }

                listOfFrames.Add(new WaitFrame(updateInterval));
            }

            listOfFrames.Add(new Frame(new Custom(Color.Black)));

            return listOfFrames;
        }

        public static List<FrameBase> SetHealthBar(int health)
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            int c;

            for (c = 3; c < (3 + (12 * (health / 100.0))); c++)
            {
                double green = (health / 100.0) * 0xff;
                double red = 0xff - ((health / 100.0) * 0xff);
                var color = Convert.ToInt32((green)) << Convert.ToInt32(8) | Convert.ToInt32(red);

                Color finalColor = new Color(Convert.ToUInt32(0x01000000 | color));

                customEffect[0, c] = finalColor;

            }

            listOfFrames.Add(new Frame(customEffect.Clone()));

            return listOfFrames;
        }

        public static List<FrameBase> ResetHealthBar()
        {
            var listOfFrames = new List<FrameBase>();
            var customEffect = Custom.Create();

            for (int i = 1; i < 100; i += 25)
            {
                var health = i;


                int c;
                for (c = 3; c < (3 + (12 * (health / i))); c++)
                {
                    double green = (health / 100.0) * 0xff;
                    double red = 0xff - ((health / 100.0) * 0xff);
                    var color = Convert.ToInt32((green)) << Convert.ToInt32(8) | Convert.ToInt32(red);

                    Color finalColor = new Color(Convert.ToUInt32(0x01000000 | color));


                    customEffect[0, c] = finalColor;

                    listOfFrames.Add(new Frame(customEffect.Clone()));
                    listOfFrames.Add(new WaitFrame(20));
                }
            }

            return listOfFrames;
        }
    }
}