﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

public class VaMemory
{
#pragma warning disable 649
    private bool _debugMode;
#pragma warning restore 649
    private IntPtr _baseAddress;
    private ProcessModule _processModule;
    private Process[] _mainProcess;
    private IntPtr _processHandle;

    public VaMemory()
    {
    }

    public VaMemory(string pProcessName)
    {
        ProcessName = pProcessName;
    }

    [Flags]
    private enum ProcessAccessFlags : uint
    {
        All = 2035711,
        Terminate = 1,
        CreateThread = 2,
        VmOperation = 8,
        VmRead = 16,
        VmWrite = 32,
        DupHandle = 64,
        SetInformation = 512,
        QueryInformation = 1024,
        Synchronize = 1048576
    }

    private enum VirtualMemoryProtection : uint
    {
        PageNoaccess = 1,
        PageReadonly = 2,
        PageReadwrite = 4,
        PageWritecopy = 8,
        PageExecute = 16,
        PageExecuteRead = 32,
        PageExecuteReadwrite = 64,
        PageExecuteWritecopy = 128,
        PageGuard = 256,
        PageNocache = 512,
        ProcessAllAccess = 2035711
    }

    private string ProcessName { get; }

    public long GetBaseAddress
    {
        get
        {
            _baseAddress = (IntPtr)0;
            _processModule = _mainProcess[0].MainModule;
            _baseAddress = _processModule.BaseAddress;
            return (long)_baseAddress;
        }
    }

    public bool CheckProcess()
    {
        if (ProcessName != null)
        {
            _mainProcess = Process.GetProcessesByName(ProcessName);
            if (_mainProcess.Length == 0)
            {
                ErrorProcessNotFound();
                return false;
            }

            _processHandle = OpenProcess(2035711U, false, _mainProcess[0].Id);
            if (!(_processHandle == IntPtr.Zero))
                return true;
            ErrorProcessNotFound();
            return false;
        }

        MessageBox.Show("Programmer, define process name first!");
        return false;
    }

    public byte[] ReadByteArray(IntPtr pOffset, uint pSize)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        var lpBuffer = new byte[(int)(IntPtr)pSize];
        try
        {
            uint lpflOldProtect;
            VirtualProtectEx(_processHandle, pOffset, (UIntPtr)pSize, 4U, out lpflOldProtect);
            ReadProcessMemory(_processHandle, pOffset, lpBuffer, pSize, 0U);
            VirtualProtectEx(_processHandle, pOffset, (UIntPtr)pSize, lpflOldProtect, out lpflOldProtect);
            return lpBuffer;
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadByteArray" + ex);
            return new byte[1];
        }
    }

    public string ReadStringUnicode(IntPtr pOffset, uint pSize)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return Encoding.Unicode.GetString(ReadByteArray(pOffset, pSize), 0, (int)pSize);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadStringUnicode" + ex);
            return string.Empty;
        }
    }

    public string ReadStringAscii(IntPtr pOffset, uint pSize)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return Encoding.ASCII.GetString(ReadByteArray(pOffset, pSize), 0, (int)pSize);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadStringASCII" + ex);
            return string.Empty;
        }
    }

    public char ReadChar(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToChar(ReadByteArray(pOffset, 1U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadChar" + ex);
            return ' ';
        }
    }

    public bool ReadBoolean(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToBoolean(ReadByteArray(pOffset, 1U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadByte" + ex);
            return false;
        }
    }

    public byte ReadByte(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return ReadByteArray(pOffset, 1U)[0];
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadByte" + ex);
            return 0;
        }
    }

    public short ReadInt16(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt16(ReadByteArray(pOffset, 2U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadInt16" + ex);
            return 0;
        }
    }

    public short ReadShort(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt16(ReadByteArray(pOffset, 2U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadInt16" + ex);
            return 0;
        }
    }

    public int ReadInt32(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt32(ReadByteArray(pOffset, 4U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadInt32" + ex);
            return 0;
        }
    }

    public int ReadInteger(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt32(ReadByteArray(pOffset, 4U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadInteger" + ex);
            return 0;
        }
    }

    public long ReadInt64(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt64(ReadByteArray(pOffset, 8U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadInt64" + ex);
            return 0;
        }
    }

    public long ReadLong(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToInt64(ReadByteArray(pOffset, 8U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadLong" + ex);
            return 0;
        }
    }

    public ushort ReadUInt16(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToUInt16(ReadByteArray(pOffset, 2U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadUInt16" + ex);
            return 0;
        }
    }

    public ushort ReadUShort(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToUInt16(ReadByteArray(pOffset, 2U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadUShort" + ex);
            return 0;
        }
    }

    public uint ReadUInt32(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToUInt32(ReadByteArray(pOffset, 4U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadUInt32" + ex);
            return 0;
        }
    }

    public uint ReadUInteger(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToUInt32(ReadByteArray(pOffset, 4U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadUInteger" + ex);
            return 0;
        }
    }

    public ulong ReadUInt64(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToUInt64(ReadByteArray(pOffset, 8U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadUInt64" + ex);
            return 0;
        }
    }

    public long ReadULong(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return (long)BitConverter.ToUInt64(ReadByteArray(pOffset, 8U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadULong" + ex);
            return 0;
        }
    }

    public float ReadFloat(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToSingle(ReadByteArray(pOffset, 4U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadFloat" + ex);
            return 0.0f;
        }
    }

    public double ReadDouble(IntPtr pOffset)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return BitConverter.ToDouble(ReadByteArray(pOffset, 8U), 0);
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: ReadDouble" + ex);
            return 0.0;
        }
    }

    public bool WriteByteArray(IntPtr pOffset, byte[] pBytes)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            VirtualProtectEx(_processHandle, pOffset, (UIntPtr)(ulong)pBytes.Length, 4U, out var lpflOldProtect);
            var flag = WriteProcessMemory(_processHandle, pOffset, pBytes, (uint)pBytes.Length, 0U);
            VirtualProtectEx(_processHandle, pOffset, (UIntPtr)(ulong)pBytes.Length, lpflOldProtect, out lpflOldProtect);
            return flag;
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteByteArray" + ex);
            return false;
        }
    }

    public bool WriteStringUnicode(IntPtr pOffset, string pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, Encoding.Unicode.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteStringUnicode" + ex);
            return false;
        }
    }

    public bool WriteStringAscii(IntPtr pOffset, string pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, Encoding.ASCII.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteStringASCII" + ex);
            return false;
        }
    }

    public bool WriteBoolean(IntPtr pOffset, bool pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteBoolean" + ex);
            return false;
        }
    }

    public bool WriteChar(IntPtr pOffset, char pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteChar" + ex);
            return false;
        }
    }

    public bool WriteByte(IntPtr pOffset, byte pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteByte" + ex);
            return false;
        }
    }

    public bool WriteInt16(IntPtr pOffset, short pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteInt16" + ex);
            return false;
        }
    }

    public bool WriteShort(IntPtr pOffset, short pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteShort" + ex);
            return false;
        }
    }

    public bool WriteInt32(IntPtr pOffset, int pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteInt32" + ex);
            return false;
        }
    }

    public bool WriteInteger(IntPtr pOffset, int pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteInt" + ex);
            return false;
        }
    }

    public bool WriteInt64(IntPtr pOffset, long pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteInt64" + ex);
            return false;
        }
    }

    public bool WriteLong(IntPtr pOffset, long pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteLong" + ex);
            return false;
        }
    }

    public bool WriteUInt16(IntPtr pOffset, ushort pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteUInt16" + ex);
            return false;
        }
    }

    public bool WriteUShort(IntPtr pOffset, ushort pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteShort" + ex);
            return false;
        }
    }

    public bool WriteUInt32(IntPtr pOffset, uint pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteUInt32" + ex);
            return false;
        }
    }

    public bool WriteUInteger(IntPtr pOffset, uint pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteUInt" + ex);
            return false;
        }
    }

    public bool WriteUInt64(IntPtr pOffset, ulong pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteUInt64" + ex);
            return false;
        }
    }

    public bool WriteULong(IntPtr pOffset, ulong pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteULong" + ex);
            return false;
        }
    }

    public bool WriteFloat(IntPtr pOffset, float pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteFloat" + ex);
            return false;
        }
    }

    public bool WriteDouble(IntPtr pOffset, double pData)
    {
        if (_processHandle == IntPtr.Zero)
            CheckProcess();
        try
        {
            return WriteByteArray(pOffset, BitConverter.GetBytes(pData));
        }
        catch (Exception ex)
        {
            if (_debugMode)
                Console.WriteLine("Error: WriteDouble" + ex);
            return false;
        }
    }

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool ReadProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        byte[] lpBuffer,
        uint dwSize,
        uint lpNumberOfBytesRead);

    [DllImport("kernel32.dll")]
    private static extern bool WriteProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        byte[] lpBuffer,
        uint nSize,
        uint lpNumberOfBytesWritten);

    [DllImport("kernel32.dll")]
    private static extern IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, int dwProcessId);

    [DllImport("kernel32.dll")]
    private static extern bool CloseHandle(IntPtr hObject);

    [DllImport("kernel32.dll")]
    private static extern bool VirtualProtectEx(
        IntPtr hProcess,
        IntPtr lpAddress,
        UIntPtr dwSize,
        uint flNewProtect,
        out uint lpflOldProtect);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr VirtualAllocEx(
        IntPtr hProcess,
        IntPtr lpAddress,
        uint dwSize,
        uint flAllocationType,
        uint flProtect);

    private void ErrorProcessNotFound()
    {
        MessageBox.Show(
            ProcessName + " is not running or has not been found. Please check and try again",
            "Process Not Found",
            MessageBoxButtons.OK,
            MessageBoxIcon.Hand);
    }
}
